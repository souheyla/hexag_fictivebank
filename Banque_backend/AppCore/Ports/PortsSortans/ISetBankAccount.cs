﻿using Banque_backend.AppCore.Domaine;

namespace Banque_backend.AppCore.Ports.PortsSortans
{
    public interface ISetBankAccount
    {
        void Set(BankAccount bankAccount);
    }
}

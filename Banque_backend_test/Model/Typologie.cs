﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Banque_backend.AppCore.Domaine
{
    [Table("typologie")]
    public class Typologie
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("parent")]
        public int Parent { get; set; }
        [Column("valeur")]
        public string Valeur { get; set; }
        [Column("date_creation")]
        public DateTime Date_creation { get; set; }
        [Column("date_maj")]
        public DateTime Date_maj { get; set; }
    }
}

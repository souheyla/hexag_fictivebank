﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Banque_backend_test.Model
{
    public class Operation
    {
        public int Id { get; set; }
        public decimal Montant { get; set; }
        public int BankAccountiD { get; set; }
        public int Typologieid { get; set; }
        public DateTime Date_creation { get; set; }
        public DateTime Date_maj { get; set; }
        public string ValeurTypologieid { get; set; }

    }
}

﻿using Banque_backend.AppCore.Domaine;

namespace Banque_backend.AppCore.Ports.PortsSortans
{
    public interface IGetBankAccount
    {
        BankAccount Get(int id) ;
    }
}

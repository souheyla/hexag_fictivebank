PGDMP                         {            banque    13.1    13.1      �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    1740217    banque    DATABASE     b   CREATE DATABASE banque WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'French_France.1252';
    DROP DATABASE banque;
                postgres    false            �            1259    1740264    client    TABLE     �   CREATE TABLE public.client (
    id integer NOT NULL,
    nom text,
    date_creation timestamp with time zone,
    date_maj timestamp with time zone,
    prenom text,
    email text,
    telephone text,
    code text,
    civilite integer
);
    DROP TABLE public.client;
       public         heap    postgres    false            �            1259    1740262    client_id_seq    SEQUENCE     �   CREATE SEQUENCE public.client_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.client_id_seq;
       public          postgres    false    203            �           0    0    client_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.client_id_seq OWNED BY public.client.id;
          public          postgres    false    202            �            1259    1740275    compte    TABLE     �   CREATE TABLE public.compte (
    id integer NOT NULL,
    code text,
    solde numeric,
    date_creation timestamp with time zone,
    date_maj timestamp with time zone,
    clientid integer,
    email text
);
    DROP TABLE public.compte;
       public         heap    postgres    false            �            1259    1740273    compte_id_seq    SEQUENCE     �   CREATE SEQUENCE public.compte_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.compte_id_seq;
       public          postgres    false    205            �           0    0    compte_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.compte_id_seq OWNED BY public.compte.id;
          public          postgres    false    204            �            1259    1740286 	   operation    TABLE     �   CREATE TABLE public.operation (
    id integer NOT NULL,
    montant numeric,
    typologieid integer,
    date_creation timestamp with time zone,
    date_maj timestamp with time zone,
    compteid integer,
    valeurtypologieid text
);
    DROP TABLE public.operation;
       public         heap    postgres    false            �            1259    1740284    operation_id_seq    SEQUENCE     �   CREATE SEQUENCE public.operation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.operation_id_seq;
       public          postgres    false    207            �           0    0    operation_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.operation_id_seq OWNED BY public.operation.id;
          public          postgres    false    206            �            1259    1740253 	   typologie    TABLE     �   CREATE TABLE public.typologie (
    id integer NOT NULL,
    parent integer,
    valeur text,
    date_creation timestamp with time zone,
    date_maj timestamp with time zone
);
    DROP TABLE public.typologie;
       public         heap    postgres    false            �            1259    1740251    typologie_id_seq    SEQUENCE     �   CREATE SEQUENCE public.typologie_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.typologie_id_seq;
       public          postgres    false    201            �           0    0    typologie_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.typologie_id_seq OWNED BY public.typologie.id;
          public          postgres    false    200            9           2604    1740267 	   client id    DEFAULT     f   ALTER TABLE ONLY public.client ALTER COLUMN id SET DEFAULT nextval('public.client_id_seq'::regclass);
 8   ALTER TABLE public.client ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    202    203    203            :           2604    1740278 	   compte id    DEFAULT     f   ALTER TABLE ONLY public.compte ALTER COLUMN id SET DEFAULT nextval('public.compte_id_seq'::regclass);
 8   ALTER TABLE public.compte ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    204    205    205            ;           2604    1740289    operation id    DEFAULT     l   ALTER TABLE ONLY public.operation ALTER COLUMN id SET DEFAULT nextval('public.operation_id_seq'::regclass);
 ;   ALTER TABLE public.operation ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    207    206    207            8           2604    1740256    typologie id    DEFAULT     l   ALTER TABLE ONLY public.typologie ALTER COLUMN id SET DEFAULT nextval('public.typologie_id_seq'::regclass);
 ;   ALTER TABLE public.typologie ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    200    201    201            �          0    1740264    client 
   TABLE DATA           l   COPY public.client (id, nom, date_creation, date_maj, prenom, email, telephone, code, civilite) FROM stdin;
    public          postgres    false    203   ""       �          0    1740275    compte 
   TABLE DATA           [   COPY public.compte (id, code, solde, date_creation, date_maj, clientid, email) FROM stdin;
    public          postgres    false    205   �"       �          0    1740286 	   operation 
   TABLE DATA           s   COPY public.operation (id, montant, typologieid, date_creation, date_maj, compteid, valeurtypologieid) FROM stdin;
    public          postgres    false    207   $#       �          0    1740253 	   typologie 
   TABLE DATA           P   COPY public.typologie (id, parent, valeur, date_creation, date_maj) FROM stdin;
    public          postgres    false    201   $       �           0    0    client_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.client_id_seq', 8, true);
          public          postgres    false    202            �           0    0    compte_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.compte_id_seq', 8, true);
          public          postgres    false    204            �           0    0    operation_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.operation_id_seq', 9, true);
          public          postgres    false    206            �           0    0    typologie_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.typologie_id_seq', 1, false);
          public          postgres    false    200            ?           2606    1740272    client pk_client 
   CONSTRAINT     N   ALTER TABLE ONLY public.client
    ADD CONSTRAINT pk_client PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.client DROP CONSTRAINT pk_client;
       public            postgres    false    203            A           2606    1740283    compte pk_compte 
   CONSTRAINT     N   ALTER TABLE ONLY public.compte
    ADD CONSTRAINT pk_compte PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.compte DROP CONSTRAINT pk_compte;
       public            postgres    false    205            C           2606    1740294    operation pk_operation 
   CONSTRAINT     T   ALTER TABLE ONLY public.operation
    ADD CONSTRAINT pk_operation PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.operation DROP CONSTRAINT pk_operation;
       public            postgres    false    207            =           2606    1740261    typologie pk_typologie 
   CONSTRAINT     T   ALTER TABLE ONLY public.typologie
    ADD CONSTRAINT pk_typologie PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.typologie DROP CONSTRAINT pk_typologie;
       public            postgres    false    201            �   �   x�}ɽ
�0@�9y
w�ܟ$���B��8.��h�����������o�WX�t�����Χ�a=�U����S�>�\�f\f)�� �N�	qi�`��JR$�G��?�mVӄZ�1���fv�o����W)�      �   U   x�3�L��-(I�7�45�0�4202�5 "C+S#+S3=c#C#SmC\��&`YCΔ̔���Ҍ̜D�����=��\1z\\\ ��      �   �   x�u�Aj�0E��)��H�$K>K7��EW-C�O��!M����g���� ������S�`�������L1�����sʔd���w��H�(��T�����5���:5�q�*�c���f�f|Ռ��,ٗT�7�y����
�VJ�f����Iɤ-pm����%��J�J�g�F�.�\�3pk��k��$m.��?Ͽ| ��:�{�9� � ��      �   �   x���M
�0�ur���~j��t�	�H�
��H��sx�*�j7�������;�^��0I�^@5�Rc+.��Om} ��jR���8�������2�ϓ��W7��K��YoA�l��|,k D�Z?Z��6��Q�ڝ�d;�;]��.w�]�֗Z�7�,o4     
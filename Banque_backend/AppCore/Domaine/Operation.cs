﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Banque_backend.AppCore.Domaine
{

    [Table("operation")]
    public class Operation
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("montant")]
        public decimal Montant { get; set; }
        [Column("compteid")]
        public int BankAccountiD { get; set; }
        [Column("typologieid")]
        public int Typologieid { get; set; }
        [Column("date_creation")]
        public DateTime Date_creation { get; set; }
        [Column("date_maj")]
        public DateTime Date_maj { get; set; }
        [Column("valeurtypologieid")]
        public string ValeurTypologieid { get; set; }

    }
}

﻿using Banque_backend.Adapter.Web;
using Banque_backend.AppCore.Ports.PortsSortans;
using Banque_backend.AppCore.Service;
using Banque_backend.AppCore.Domaine;
using log4net;
using System.Linq;

namespace Banque_backend.Adapter.Persistance
{
    public class BankAccountRepository : IGetBankAccount, ISetBankAccount, IGetOperations,IAddOperation
    {
        private BankAccountDbContext Context { get; }

        private BankAccountRepository()
        {
            this.Context= new BankAccountDbContext();
        }
        //private BankAccountDbContext Context { get; }

        public BankAccountRepository(BankAccountDbContext Context)
        {
            this.Context = Context;
        }

        public BankAccount Get(int id)
        {
            var  db = Context;
            return db.BankAccount.Where(w => w.Id == id).Single();
            
        }

        public void Set(BankAccount bankAccount)
        {
            var db = Context;
            db.BankAccount.Update(bankAccount);
            db.SaveChanges();
        }

        public List<Operation> GetOps(int id)
        {
            var db = Context;
            var ops = db.Operations.Where(x => x.BankAccountiD == id).OrderByDescending(x=>x.Id).ToList();
            return ops;
        }

        public void AddOp(Operation operation)
        {
            var  db = Context;
            db.Operations.Add(operation);
            db.SaveChanges();
        }
    }

}

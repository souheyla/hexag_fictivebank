﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Banque_backend.AppCore.Domaine
{
    [Table("compte")]
    public class BankAccount
    {

        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("code")]
        public string Code { get; set; }

        [Column("email")]
        public string Email { get; set; }

        [Column("solde")]
        public decimal Solde { get; set; }
        [Column("date_maj")]
        public DateTime Date_maj { get; set; }
        [Column("date_creation")]
        public DateTime Date_creation { get; set; }

        public List<Operation> Operations { get; set; }

        public BankAccount()
        {
            this.Operations = new List<Operation>();
        }

        public IEnumerable<Operation> GetHistorique(int compteid)
        {
            return this.Operations;
        }

        public string SetRetrait(decimal montant)
        {
            
            if (Solde >= montant && montant > 0)
            {
                this.Solde = this.Solde - montant;
                return "Votre compte a été débité d'un montant de :" + montant + "€";
            }
            else 
            {
                return "Votre solde est inssufisant. ";
            }

        }

        public string SetDepot(decimal montant)
        {

            if (montant > 0)
            {
                this.Solde += montant;
                return "Votre compte a été crédité d'un montant de : " + montant + "€";
            }
            else
            {
                return "Une erreur s'est produite veuillez contacter votre conseiller.";
            }
        }

        public decimal GetSolde()
        {
            return this.Solde;
        }

        
    }
}

﻿namespace Banque_backend.AppCore.Ports.PortsEntrants
{
    public interface ISetDepotUseCase
    {
        string SetDepot(int compteid, decimal montant);
    }
}

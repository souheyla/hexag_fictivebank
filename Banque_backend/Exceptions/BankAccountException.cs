﻿
namespace Banque_backend.Exceptions
{
    [Serializable]
    public class BankAccountException
    {
        
        class SoldeInsufisantException : Exception
        {
            public SoldeInsufisantException() : base() { }
            public override string Message                        
            {
                get
                {
                    return "Votre solde est inssufisant";
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banque_backend_test.Model
{

    public class MockBankRepo:IBankAccount
    {
        private int Id { get; set; }
        private string Code { get; set; }
        private string Email { get; set; }
        private decimal Solde { get; set; }
        private DateTime Date_maj { get; set; }
        private DateTime Date_creation { get; set; }

        private List<Operation> Operations { get; set; }

        public MockBankRepo()
        {
            this.Id = 1;
            this.Code = "000000";
            this.Solde = 300;
            this.Date_creation = DateTime.Now;
            this.Date_maj = DateTime.Now;
            this.Operations = new List<Operation>()
            {
                new Operation()
                {
                    Date_maj = DateTime.Now,
                    Date_creation = DateTime.Now,
                    BankAccountiD = 1,
                    Montant = 300,
                    Typologieid =3
                }
            };
        }

        public IEnumerable<Operation> GetHistorique(int compteid)
        {
            return this.Operations;
        }

        public string SetRetrait(decimal montant)
        {
            
            if (Solde >= montant && montant > 0)
            {
                this.Solde = this.Solde - montant;
                return "Votre compte a été débité d'un montant de :" + montant + "€";
            }
            else 
            {
                return "Votre solde est inssufisant. ";
            }

        }

        public string SetDepot(decimal montant)
        {
            if (montant > 0)
            {
                this.Solde += montant;
                return "Votre compte a été crédité d'un montant de : " + montant + "€";
            }
            else
            {
                return "Une erreur s'est produite veuillez contacter votre conseiller.";
            }
        }

        public decimal GetSolde()
        {
            return this.Solde;
        }

        
    }
}

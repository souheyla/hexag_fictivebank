﻿using Banque_backend.AppCore.Domaine;

namespace Banque_backend.AppCore.Ports.PortsSortans
{
    public interface IGetOperations
    {
        List<Operation> GetOps(int id);
    }
}

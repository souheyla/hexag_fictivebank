﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Banque_backend_test.Model;

namespace Banque_backend_test.Model
{
    internal interface IBankAccount
    {
        IEnumerable<Operation> GetHistorique(int compteid);
        string SetRetrait(decimal montant);
        string SetDepot(decimal montant);
        decimal GetSolde();
    }
}

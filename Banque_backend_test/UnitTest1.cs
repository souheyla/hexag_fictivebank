using Banque_backend.Adapter.Persistance;
using Banque_backend.Adapter.Web;
using Banque_backend.AppCore.Domaine;
using Banque_backend.AppCore.Service;
using Banque_backend_test.Model;

namespace Banque_backend_test
{
    [TestClass]
    public class UnitTest1
    {
        private readonly CompteBancaireService cbs=new (new BankAccountRepository(new BankAccountDbContext()));

        private readonly MockBankRepo b = new();
        [TestMethod]
        public void VerifBankAccount()
        {
            var q = b.GetSolde();
            Assert.IsTrue(q == 300);

            var ops = b.GetHistorique(1);
            Assert.AreEqual(ops.Count(), 1);

            var r = b.SetRetrait( 5);
            Assert.AreEqual(r, "Votre compte a �t� d�bit� d'un montant de :" + 5 + "�");

            var rr = b.SetRetrait(500);
            Assert.IsFalse(rr == "Votre compte a �t� d�bit� d'un montant de :" + 500 + "�");

            Assert.AreEqual(rr , "Votre solde est inssufisant. ");

            var d = b.SetDepot( 5000);
            Assert.AreEqual(d, "Votre compte a �t� cr�dit� d'un montant de : " + 5000 + "�");

            var dd = b.SetDepot(-50);
            Assert.AreEqual(dd, "Une erreur s'est produite veuillez contacter votre conseiller.");

            var ddd = b.SetDepot(0);
            Assert.AreEqual(dd, "Une erreur s'est produite veuillez contacter votre conseiller.");

        }
    }
}
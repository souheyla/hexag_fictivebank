﻿using Banque_backend.AppCore.Domaine;

namespace Banque_backend.AppCore.Ports.PortsEntrants
{
    public interface IGetHistoriqueUseCase
    {
        IEnumerable<Operation> GetHistorique(int compteid);
    }
}

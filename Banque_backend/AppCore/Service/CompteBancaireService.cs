﻿using Banque_backend.AppCore.Ports.PortsEntrants;
using Banque_backend.AppCore.Ports.PortsSortans;
using Banque_backend.AppCore.Domaine;
using Banque_backend.Adapter.Persistance;
using Banque_backend.Adapter.Web;

namespace Banque_backend.AppCore.Service
{
    public class CompteBancaireService : IGetSoldeUseCase, IGetHistoriqueUseCase, ISetDepotUseCase, ISetRetraitUseCase
    {
        private readonly IGetBankAccount getBankAccount;
        private readonly ISetBankAccount setBankAccount;
        private readonly IGetOperations getOperations;
        private readonly IAddOperation addOperation;

        public CompteBancaireService(BankAccountRepository repo) {
            this.getOperations = repo;
            this.getBankAccount = repo;
            this.setBankAccount = repo;
            this.addOperation=repo;
        }
       
        public IEnumerable<Operation> GetHistorique(int compteid)
        {
            BankAccount account = getBankAccount.Get(compteid);
            List<Operation> ops = getOperations.GetOps(compteid);
            account.Operations = ops;
            return account.GetHistorique(compteid);
        }
        public BankAccount GetCompte(int compteid)
        {
            BankAccount account = getBankAccount.Get(compteid);
            return account;
        }

        public decimal GetSolde(int compteid)
        {
            BankAccount account = getBankAccount.Get(compteid);
            return account.GetSolde();
        }

        public string SetDepot(int compteid, decimal montant)
        {
            if(montant>0)
            {
                BankAccount account = getBankAccount.Get(compteid);
                account.SetDepot(montant);
                setBankAccount.Set(account);

                Operation o = new ()
                {
                    Date_maj = DateTime.Now,
                    Date_creation = DateTime.Now,
                    BankAccountiD = compteid,
                    Montant = montant,
                    Typologieid = 3,
                    ValeurTypologieid="depot"
                };

                addOperation.AddOp(o);

                return "Votre compte a été crédité d'un montant de : " + montant + "€";
            }
            else
            {
                return "Une erreur s'est produite veuillez contactez votre conseiller, merci.";
            }
            
        }

        public string setRetrait(int compteid, decimal montant)
        {
            BankAccount account = getBankAccount.Get(compteid);
            if (account.Solde >= montant) { 
                account.SetRetrait(montant);
                setBankAccount.Set(account);


                Operation o = new ()
                {
                    Date_maj = DateTime.Now,
                    Date_creation = DateTime.Now,
                    BankAccountiD = compteid,
                    Montant = montant,
                    Typologieid = 2,
                    ValeurTypologieid="retrait"
                };

                addOperation.AddOp(o);
                return "Votre compte a été débité d'un montant de : " + montant + "€";
            }
            else
            {
                return "Votre solde est inssufisant. ";
            }
        }
       
    }
}

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Banque_backend.AppCore.Service;
using Banque_backend.Adapter.Web;
using Banque_backend.Adapter.Persistance;
using Banque_backend.AppCore.Domaine;
using Banque_backend.Exceptions;

namespace Banque_backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BankAccountController : ControllerBase
    {
        //**************************************************************************************************
       
        private BankAccountDbContext Context { get; }
        private readonly IConfiguration _config;
        private string mon_token;
        private readonly CompteBancaireService cbs;
        //**************************************************************************************************
        public BankAccountController(IConfiguration config)
        {
            this.Context = new BankAccountDbContext();
            _config = config;
            cbs=new CompteBancaireService(new BankAccountRepository(this.Context));
        }

        //**************************************************************************************************
        private string GenerateToken(UserLogin user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier,user.Email),
                new Claim(ClaimTypes.Role,user.Code)
            };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                _config["Jwt:Audience"],
                claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: credentials
            );


            return new JwtSecurityTokenHandler().WriteToken(token);

        }

        [HttpGet("session/{email}/{code}")]
        public ActionResult GetSession(string email, string code)
        {
            using var db = Context;
            
            var token = GenerateToken(new UserLogin(email, code));
            mon_token = token;
            return Ok(token);
            

        }

        //operations/////////////////////////////////////////////////////////////////////////////////////////////////////////
        [HttpGet("operations/{compteid}")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IEnumerable<Operation> GetHistorique(int compteid)
        {
            return cbs.GetHistorique(compteid);
        }

        [HttpGet("solde/{compteid}")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public decimal GetSolde(int compteid)
        {
            return cbs.GetSolde(compteid);
        }

        [HttpGet("compte/{compteid}")]
        public BankAccount GetCompte(int compteid)
        {
            return cbs.GetCompte(compteid);
        }

        [HttpPut("retrait/{compteid}/{montant}")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public string SetRetrait( int compteid, decimal montant)
        {
            var q= cbs.setRetrait(compteid, montant);
            return q;        
        }


        [HttpPut("depot/{compteid}/{montant}")]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public string SetDepot(int compteid,decimal montant)
        {
            var q= cbs.SetDepot(compteid, montant);
            return q;
            
        }
    }
}
﻿namespace Banque_backend.AppCore.Ports.PortsEntrants
{
    public interface ISetRetraitUseCase
    {
        string setRetrait(int compteid, decimal montant);
    }
}

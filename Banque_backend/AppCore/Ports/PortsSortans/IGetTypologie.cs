﻿using Banque_backend.AppCore.Domaine;

namespace Banque_backend.AppCore.Ports.PortsSortans
{
    public interface IGetTypologie
    {
        string Get(int id);
    }
}

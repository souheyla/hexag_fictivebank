﻿namespace Banque_backend.AppCore.Ports.PortsEntrants
{
    public interface IGetSoldeUseCase
    {
        decimal GetSolde(int compteid);
    }
}
